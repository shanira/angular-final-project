import { Routes } from '@angular/router';
import { AuthGuard } from './app/auth.guard';
import { SignupFormComponent } from './app/signup-form/signup-form.component';
import { LoginFormComponent } from './app/login-form/login-form.component';
import { ChatroomComponent } from './app/chatroom/chatroom.component';
import { ProfileComponent } from './app/profile/profile.component';
import { UserComponent } from './app/user/user.component';

export const appRoutes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full'},
    { path: 'signup', component: SignupFormComponent },
    { path: 'login', component: LoginFormComponent },
    { path: 'chat', component: ChatroomComponent, canActivate: [AuthGuard] },
    { path: 'profile/:displayname', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'user/:username/:userid', component: UserComponent, canActivate: [AuthGuard] },
];
