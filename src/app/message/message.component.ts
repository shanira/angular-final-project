import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { AuthService } from '../services/auth.service';
import { ChatMessage } from '../models/chat-message.model';
import { UploadService } from '../services/upload.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  photoUrl: string;

  @Input() chatMessage: ChatMessage;
  key: string;
  userEmail: string;
  userName: string;
  uid: string;
  messageContent: string;
  timeStamp: Date = new Date();
  isOwnMessage: boolean;
  ownEmail: string;

  constructor(private authService: AuthService, private uploadService: UploadService) {
    authService.authUser().subscribe(user => {
      this.ownEmail = user.email;
      this.isOwnMessage = this.ownEmail === this.userEmail;
    });
  }

  ngOnInit(chatMessage = this.chatMessage) {
    this.key = chatMessage.$key;
    this.messageContent = chatMessage.message;
    this.timeStamp = chatMessage.timeSent;
    this.userEmail = chatMessage.email;
    this.userName = chatMessage.userName;
    this.uid = chatMessage.uid;
    this.uploadService.getImages(chatMessage.userName).subscribe( a => {
      const photo = a.photo;
      if (photo != null) {
        this.photoUrl = a.photo.slice(0, -4);
      }else {
        this.photoUrl = 'default';
      }
    });
    // console.log(this.uid);
  }
}
