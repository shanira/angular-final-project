import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ReactionService } from '../services/reaction.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-reaction',
  templateUrl: './reaction.component.html',
  styleUrls: ['./reaction.component.css']
})

export class ReactionComponent implements OnInit, OnDestroy {

  @ViewChild('audioOption') audioPlayerRef: ElementRef;
  @Input() itemId: string;
  showEmojis = false;
  emojiList: string[];
  reactionCount: any;
  userReaction: any;
  subscription: any;

  constructor(private reactionSvc: ReactionService) { }

  ngOnInit() {
    this.emojiList = this.reactionSvc.emojiList;

    this.subscription = this.reactionSvc.getReactions(this.itemId).subscribe(reactions => {
        this.reactionCount = this.reactionSvc.countReactions(reactions);
        this.userReaction  = this.reactionSvc.userReaction(reactions);
    });
  }
  onAudioPlay() {
    this.audioPlayerRef.nativeElement.play();
  }

  react(val) {
    this.onAudioPlay();
    if (this.userReaction === val) {
      this.reactionSvc.removeReaction(this.itemId);
    } else {
      this.reactionSvc.updateReaction(this.itemId, val);
    }
  }

  toggleShow() {
    this.showEmojis = !this.showEmojis;
  }

  emojiPath(emoji) {
   return `assets/reactions/${emoji}.svg`;
  }

  hasReactions(index) {
    return _.get(this.reactionCount, index.toString());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
