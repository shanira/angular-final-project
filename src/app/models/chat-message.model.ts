export class ChatMessage {
    $key?: string;
    email?: string;
    userName?: string;
    uid?: string;
    message?: string;
    timeSent?: Date = new Date();
}
