import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { UploadService } from '../services/upload.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  errors: Array<string> = [];
  dragAreaClass = 'dragarea';
  @Input() displayName: string;
  @Input() fileExt = 'JPG, GIF, PNG';
  @Input() maxFiles = 5;
  @Input() maxSize = 5;
  @Output() uploadStatus = new EventEmitter();

  constructor(private uploadService: UploadService) { }

  ngOnInit() { }

  onFileChange(event) {
    const files = event.target.files;
    this.saveFiles(files, this.displayName);
  }
  saveFiles(files, displayName) {
    this.errors = [];
    const formData: FormData = new FormData();
    formData.append('uploads', files[0]);
    this.uploadService.upload(formData, displayName)
      .subscribe(
        success => {
          this.uploadStatus.emit(true);
          console.log(success);
        },
        error => {
            this.uploadStatus.emit(true);
            this.errors.push(error.ExceptionMessage);
        }
      );
  }
}
