import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'angular2-moment';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ChatFormComponent } from './chat-form/chat-form.component';
import { ChatroomComponent } from './chatroom/chatroom.component';
import { FeedComponent } from './feed/feed.component';
import { MessageComponent } from './message/message.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserItemComponent } from './user-item/user-item.component';

import { ChatService } from './services/chat.service';
import { AuthService } from './services/auth.service';

import { appRoutes } from '../routes';
import { environment } from '../environments/environment';
import { AuthGuard } from './auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { UserComponent } from './user/user.component';
import { ReactionComponent } from './reaction/reaction.component';
import { ReactionService } from './services/reaction.service';
import { FileComponent } from './file/file.component';
import { UploadService } from './services/upload.service';
import { NumbersPipe } from './numbers.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ChatFormComponent,
    ChatroomComponent,
    FeedComponent,
    MessageComponent,
    LoginFormComponent,
    SignupFormComponent,
    UserListComponent,
    UserItemComponent,
    ProfileComponent,
    UserComponent,
    ReactionComponent,
    FileComponent,
    NumbersPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    AngularFireModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpModule,
    HttpClientModule,
    MomentModule
  ],
  providers: [AuthService, AuthGuard, ChatService, ReactionService, UploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }
