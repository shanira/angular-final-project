import { Component, OnInit} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { FirebaseObjectObservable } from 'angularfire2/database';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user: Observable<firebase.User>;
  userEmail: string;
  userName: string;
  userInfo: FirebaseObjectObservable<any>;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.authUser();
    this.user.subscribe(user => {
      if (user) {
        this.userEmail = user.email;
        this.userInfo = this.authService.getUser(user.uid);
        this.userInfo.subscribe(info => {
          this.userName = info.displayName;
        });
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
