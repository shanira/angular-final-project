import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Http, Headers, RequestOptions, Response } from '@angular/http';


@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent {
  fullName: string;
  email: string;
  password: string;
  displayName: string;
  phone: string;
  address: string;
  city: string;
  state: string;
  errorMsg: string;

  constructor(private authService: AuthService, private router: Router, public http: Http) { }

  signUp() {
    this.authService.signUp(this.email, this.password, this.displayName)
      .then(resolve => {
        this.authService.logout();        
        this.router.navigate(['login']);
      })
      .catch(error => this.errorMsg = error.message);
    this.http.post('../server/api/user/add', { full_name: this.fullName, email: this.email, password: this.password,
        display_name: this.displayName, phone: this.phone, address: this.address, city: this.city, state: this.state })
        .subscribe(data => console.log(data),err => console.log(err));
  }
}
