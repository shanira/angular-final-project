import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numbers'
})
export class NumbersPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (value === 0) {
      return 0;
    }
    if (value <= 999) {
        return value ;
    } else if (value >= 1000 && value <= 999999) {
        return (value / 1000) + 'K';
    } else if (value >= 1000000 && value <= 999999999) {
        return (value / 1000000) + 'M';
    } else if (value >= 1000000000 && value <= 999999999999) {
        return (value / 1000000000) + 'B';
    } else {
        return value ;
    }
  }

}
