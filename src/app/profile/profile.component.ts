import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { UploadService } from '../services/upload.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  errorMessage: string;
  images: Array<any>= [];
  photoUrl: any;

  constructor(public http: Http, private _acroute: ActivatedRoute, private uploadService: UploadService) { }
  userName: string;
  id: number;
  full_name: string;
  email: string;
  phone: string;
  address: string;
  city: string;
  state: string;

  ngOnInit() {
    this.userName = this._acroute.snapshot.params['displayname'];
    const token = localStorage.getItem('token');
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${token}`);
    const options = new RequestOptions({ headers: headers });
    this.http.get('../server/api/userinfo/' + this.userName, options)
      .map(data => data.json())
      .subscribe(r => {
        this.id = r.id;
        this.full_name = r.full_name;
        this.email = r.email;
        this.phone = r.phone;
        this.address = r.address;
        this.city = r.city;
        this.state = r.state;
      });

    this.getImageData();
  }

  getImageData() {
    this.uploadService.getImages(this.userName).subscribe(a => {
      console.log(a);
      const photo = a.photo;
      if (photo != null) {
        this.photoUrl = a.photo.slice(0, -4);
      }else {
        this.photoUrl = 'default';
      }
    });
  }

  refreshImages(status) {
    if (status === true) {
      this.getImageData();
    }
  }

  update() {
    const token = localStorage.getItem('token');
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${token}`);
    const options = new RequestOptions({ headers: headers });
    this.http.post('../server/api/user/update/' + this.userName,
                                                              {
                                                                full_name: this.full_name,
                                                                email: this.email,
                                                                phone: this.phone,
                                                                address: this.address,
                                                                city: this.city,
                                                                state: this.state
                                                              }, options)
    .map(data => data.json())
    .subscribe(log => this.errorMessage = log.notice.text, err => console.log(err));
  }
}
