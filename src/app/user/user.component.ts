import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { Observable } from 'rxjs/Observable';
import { ChatMessage } from '../models/chat-message.model';
import { FirebaseListObservable } from 'angularfire2/database';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { UploadService } from '../services/upload.service';
import { User } from '../models/user.model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  msgs: FirebaseListObservable<ChatMessage[]>;
  photoUrl: string;
  constructor(
    private chat: ChatService,
    private afauth: AuthService,
    public http: Http,
    private _acroute: ActivatedRoute,
    private uploadService: UploadService) { }
  userName: any;
  id: number;
  full_name: string;
  email: string;
  phone: string;
  address: string;
  city: string;
  state: string;
  userid: string;
  popularity: number;

  ngOnInit() {
    this.userName = this._acroute.snapshot.params['username'];
    this.userid = this._acroute.snapshot.params['userid'];
    this.afauth.incrementPop(this.userid);
    this.afauth.getPop(this.userid).subscribe(a => {
      this.popularity = a['$value'];
      console.log(a);
    });
    this.msgs = this.chat.getAllMessages();
    const token = localStorage.getItem('token');
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${token}`);
    const options = new RequestOptions({ headers: headers });
    this.http.get('../server/api/userinfo/' + this.userName, options)
      .map(data => data.json())
      .subscribe(r => {
        this.id = r.id;
        this.full_name = r.full_name;
        this.email = r.email;
        this.phone = r.phone;
        this.address = r.address;
        this.city = r.city;
        this.state = r.state;
      });
      this.uploadService.getImages(this.userName).subscribe(a => {
        const photo = a.photo;
        if (photo != null) {
          this.photoUrl = a.photo.slice(0, -4);
        }else {
          this.photoUrl = 'default';
        }
      });
  }
}
