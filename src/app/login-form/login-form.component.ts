import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
  email: string;
  password: string;
  errorMsg: string;

  constructor(private authService: AuthService, private router: Router, private http: Http) { }

  login() {
      this.http.post('../server/api/login', { email: this.email, password: this.password })
        .map(res => res.json())
        .subscribe(
          data => localStorage.setItem('token', data.token),
          err => console.log('Sorry ! An error occured :(')
        );
      this.authService.login(this.email, this.password).catch(error => this.errorMsg = error.message);
  }
}
