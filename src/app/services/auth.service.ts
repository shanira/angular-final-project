import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user.model';

@Injectable()
export class AuthService {
  private user: Observable<firebase.User>;
  private authState: any;
  displayName: string;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router,
    public http: Http
  ) {
      this.user = afAuth.authState;
    }

    authUser() {
      return this.user;
    }

    get currentUserId(): string {
      return this.authState !== null ? this.authState.uid : '';
    }

    getUser(uid) {
      const userId = uid;
      const path = `/users/${userId}`;
      return this.db.object(path);
    }
    getPop(uid) {
      const userId = uid;
      const path = `/users/${userId}/popularity`;
      return this.db.object(path);
    }

    login(email: string, password: string) {
      return this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then((user) => {
          this.authState = user;
          this.setUserStatus('online');
          this.router.navigate(['chat']);
        });
    }

    logout() {
      this.afAuth.auth.signOut();
      this.setUserStatus('offline');
      localStorage.removeItem('token');
      this.router.navigate(['login']);
    }

    signUp(email: string, password: string, displayName: string) {
      return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
              .then((user) => {
                this.authState = user;
                const status = 'online';
                const popularity = 0;
                this.setUserData(email, displayName, status, popularity);
              }).catch(error => console.log(error));
    }

    setUserData(email: string, displayName: string, status: string, popularity: number): void {
      const path = `users/${this.currentUserId}`;
      const data = {
        email: email,
        displayName: displayName,
        status: status
      };

      this.db.object(path).update(data)
        .catch(error => console.log(error));
    }

    incrementPop(user) {
      this.db.object(`users/${user}/popularity`).$ref
      .ref.transaction(popularity => {
          if (popularity === null) {
              return popularity = 1;
          } else {
              return popularity + 1;
          }
      });
    }

    setUserStatus(status: string): void {
      const path = `users/${this.currentUserId}`;
      const data = {
        status: status
      };

      this.db.object(path).update(data)
        .catch(error => console.log(error));
    }
    loggedIn() {
      return !!localStorage.getItem('token');
    }
}
