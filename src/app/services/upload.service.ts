import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UploadService {

  _baseURL = '../server/api/upload/';
  constructor(private http: Http) { }

  upload(files, displayName) {
    // console.log(displayName);
    const token = localStorage.getItem('token');
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${token}`);
    const options = new RequestOptions({ headers: headers });
      return this.http.post(this._baseURL + displayName, files, options)
                .map(response => response.json())
                .catch(error => Observable.throw(error));
  }

  getImages(displayName) {
    const token = localStorage.getItem('token');
    const headers = new Headers();
    headers.set('Authorization', `Bearer ${token}`);
    const options = new RequestOptions({ headers: headers });
    return this.http.get('../server/api/propic/' + displayName, options)
      .map(data => data.json()).catch(error => Observable.throw(error));
  }
}
