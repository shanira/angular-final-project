import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from '../services/chat.service';
import { User } from '../models/user.model';
import { UploadService } from '../services/upload.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {

  photoUrl: string;
  userid: string;

  @Input() user: User;

  constructor(private chatService: ChatService, private uploadService: UploadService) { }

  ngOnInit() {
    this.userid = this.user['$key'];
    this.uploadService.getImages(this.user.displayName).subscribe(a => {
      const photo = a.photo;
      if (photo != null) {
        this.photoUrl = a.photo.slice(0, -4);
      }else {
        this.photoUrl = 'default';
      }
    });
  }

}
